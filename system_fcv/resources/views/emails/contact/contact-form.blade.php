@component('mail::message')
# Introduction

The body of your message.

<p>Client Name:  </p>{{$data['yourname']}}<br><br>  

<p>Client Email: </p>{{$data['email']}}<br><br>  

<p>Client Visa Type: </p>{{$data['selectvisa']}}<br><br>  

<p>CLient Message: </p>{{$data['message']}}<br><br>                                    


Thanks,<br>
{{ config('app.name') }}
@endcomponent
