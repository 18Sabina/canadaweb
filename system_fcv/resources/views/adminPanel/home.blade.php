@extends('layout.templateadmincontrol')


@section('title','Canada Visa | Admin Control')

@section("content")
<h1>Admin control Panel</h1>
@if($message = Session::get('success'))
    <div class="alert alert-sucess">
        <p>{{$message}}</p>
    </div>
@endif
@if($errors->any())
 <div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error)
        <li>{{$error}} </li>
        @endforeach
    </ul>
 </div>
@endif

<a href="{{ route('post.create') }}" class="btn btn-primary pull-rigth">Add New post</a>
<table class="table">
    <thead>
        <th>id</th>
        <th>title</th>
        <th>body</th>
        <th>edit</th>
        <th>remove</th>
    </thead>
    <tbody>
    @foreach($posts as $post)
    <tr>
        <th>{{$post->id ?? ''}}</th>
        <td>{{$post->title ?? ''}}</td>
        <td>{{$post->body ?? ''}}</td>
        <td>
            <a href="{{ route('post.edit', $post->id)}}" class="btn btn-primary">Edit</a>
        </td>
        <td>
            <form action="{{ route('post.destroy',$post->id )}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="DELETE">
                <input class="btn btn-danger" type = "submit" value="Delete">
            </form>
        </td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection

