@extends('layout.templateadmincontrol')

@section('title','CanadaVisa | Add Blog')

@section("head_section")
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
@endsection

@section('content')
<script>
        tinymce.init({
        selector: '#mytextarea',
        height: 400,
        width : 900,
        theme: 'modern',
        plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount  imagetools  contextmenu colorpicker textpattern help',
        toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
        });
  </script>

<h1>Add New Post</h1>
<div class="col-sm-8 col-sm-offset-2">
    <form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
    @csrf
        <div class="form-group">
            <label for="title">Title:</label>
            <input name="title" type="text" class="form-control">
            </div>
        <div class="form-group">
            <input type="file" name="image" />
        </div>
      
        <div class="form-group">
            <label for="body">Body:</label>
           <textarea name="body" id="mytextarea"  class="form-control"></textarea>
        </div>
        <button type="submit" class="submit btn btn-primary" >Submit</button>
        <a href="{{ route('post.index') }}" class="btn btn-default pull-rigth">Go Back</a>

    </form>
</div>

@endsection