

<div class="container">
    <div class="row">
        <a href="{{url('/blog')}}">Go Back Home</a>
    </div>
</div>

<div class="container">
    <div class="row">
        <h1>{{$post->title}}</h1>
        <p>{{$post->body}}</p>
    </div>
</div>

<span class="fb-comments-count" data-href="{{ Request::url()}}"></span>
<!-- this need to be after body tag -->
<!-- facebook comment plugins -->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v7.0&appId=259676348450816&autoLogAppEvents=1" nonce="8M0EpPRs"></script>
 
<!-- this need to be after body tag -->
<div class="fb-comments" data-href="http://127.0.0.1:8000/post/1" data-numposts="10" data-width=""></div>
<!-- <div class="fb-comments" data-href="http://127.0.0.1:8000/post/{{$id}}" data-numposts="10" data-width=""></div> -->
