@extends("layout.html")

@section("title","Free Assesment | Canada Visa")


@section("assesmentonly");

        <div class="content">
            <div class="container">
                <div class="row">
                    <div class= "col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">

                        <h1>A free assessment of your visa options with our visa check for Canada</h1>
                       
                            <div class="alert alert-info " style=" margin:10px;">
                                <strong>Info!</strong> Are you dreaming of a future in Canada? Start your adventure by filling out our free and non-binding visa check.
                        For who?
                            </div>
                        <p><br><br>

                        This visa check is for people who would live and work in Canada temporarily or permanently. Do you intend to run a business in Canada? Then please fill out the visa check for business purposes.
                        How does it work?<br><br>

                        Please fill out the form below if you are searching for an advice on your visa options for Canada. Based on the information you provide in the visa check form, our visa consultants will assess your profile and therewith your visa options. Please note that this is not an automated process, therefore it could take some time to assess your profile.

                        <br><br>
                        </p>
                            <div class="alert alert-success">
                              <strong>Success!</strong> Once an assessment is made, our consultants will contact you by phone or e-mail to provide you with an advice.
                                    Please keep an eye on your ‘spam-box’. It might happen that our e-mails are received there.
                            </div> 
                    </div>

                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    @if($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <strong>Notice:</strong> {{$message}}
                                </div>
                            @endif
                        <div class="sidebar">
                            <div class="widget widget-quote-form bg-yellow">
                                <h3 class="form-title">Free Immigration Assessment</h3>

                                <p class="form-text">Find out your options for visa by completing a free online assessment.</p>
                                <form class="sidebar-quote-form" method="post" action="/mail">
                                {{csrf_field()}}
                                     Name :
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="yourname">Your Name</label>
                                        <div class="">
                                            <input id="yourname" name="yourname" type="text" placeholder="Your Name" class="form-control" required="">
                                        </div>
                                    </div>
                                     Email :
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="email">Email</label>
                                        <div class="">
                                            <input id="email" name="email" type="email" placeholder="Email" class="form-control input-md" required="">
                                        </div>
                                    </div>
                                  Contact No:
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="mobileno">Mobile No</label>
                                        <div class="">
                                            <input id="mobileno" name="mobileno" type="number" placeholder="Mobile No" class="form-control" required="">
                                        </div>
                                    </div>
                                    Visa Type:
                                    <div class="form-group">
                                        <label for="selectvisa" class="sr-only">Select Visa</label>
                                        <select class="form-control" id="selectvisa" name="selectvisa">
                                            <option value="Select Visa">Select Visa</option>
                                            <option value="Students Visa">Students Visa</option>
                                            <option value="Business Visa">Business Visa</option>
                                            <option value="Family Visa">Family Visa</option>
                                            <option value="Travel Visa">Travel Visa </option>
                                            <option value="Work Visa">Work Visa </option>
                                            <option value="Visitor Visa">Visitor Visa </option>
                                            <option value="Migrate Visa">Migrate Visa </option>
                                            <option value="PR Visa">PR Visa</option>
                                        </select>
                                    </div>
                                    Message :
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="message">Message</label>
                                        <div class="">
                                            <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-lg btn-block">Book Free Assessment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
