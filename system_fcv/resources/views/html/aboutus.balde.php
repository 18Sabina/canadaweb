@extends("layout.html")

@section("title","About Us | Canda Visa")


@section("assesmentonly");

        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="sidebar">
                            <div class="widget widget-quote-form bg-yellow">
                                <h3 class="form-title">Free Immigration Assessment</h3>
                                <p class="form-text">Find out your options for visa by completing a free online assessment.</p>
                                <form class="sidebar-quote-form" method="post" action="widget-enquiry.php">
                                     Text input
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="yourname">Your Name</label>
                                        <div class="">
                                            <input id="yourname" name="yourname" type="text" placeholder="Your Name" class="form-control" required="">
                                        </div>
                                    </div>
                                     Text input
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="email">Email</label>
                                        <div class="">
                                            <input id="email" name="email" type="email" placeholder="Email" class="form-control input-md" required="">
                                        </div>
                                    </div>
                                  
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="mobileno">Mobile No</label>
                                        <div class="">
                                            <input id="mobileno" name="mobileno" type="number" placeholder="Mobile No" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="selectvisa" class="sr-only">Select Visa</label>
                                        <select class="form-control" id="selectvisa" name="selectvisa">
                                            <option value="Select Visa">Select Visa</option>
                                            <option value="Students Visa">Students Visa</option>
                                            <option value="Business Visa">Business Visa</option>
                                            <option value="Family Visa">Family Visa</option>
                                            <option value="Travel Visa">Travel Visa </option>
                                            <option value="Work Visa">Work Visa </option>
                                            <option value="Visitor Visa">Visitor Visa </option>
                                            <option value="Migrate Visa">Migrate Visa </option>
                                            <option value="PR Visa">PR Visa</option>
                                        </select>
                                    </div>
                                    <!-- Textarea -->
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="message">Message</label>
                                        <div class="">
                                            <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-lg btn-block">Book Free Assessment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
