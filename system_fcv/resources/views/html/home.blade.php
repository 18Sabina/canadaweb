@extends("layout.html")

@section("title","Home | Canada Visa")

@section("page-header");
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <h1 class="page-title">Canada Immigration -<br> Apply CAIPS/ FOSS/GCMS</h1>
                    <p class="page-description">you have applied for a visa, and the application was turned down. <br><br> Now what? The Visa Officer’s notes of your case file sheds light on the reason for the refusal</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("reject");
        <div class="content">
            <div class="container">
                <div class="content-area">
                    <h2>Visa Rejected </h2>
                            
                        <div class="row">
                                  <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Apply CAIPS</h3>
                                            <p>CAIPS stands for “Computer Assisted Immigration Processing System”. It is a computer system used by all Canadian visa Offices/Consulates to process visa applications. It has computerized record of all visa applications including remarks and interviews taken by the high commission in past from outside Canada.</p>
                                            <a href="{{url('/applycaips')}}" class="btn-link-primary">Apply Now</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Apply FOSS</h3>
                                            <p>FOSS stands for “Field Operational Support System”. It is the computer system used by CIC offices in Canada to track all immigration related information. It is mainly used for applications made from inside Canada.FOSS also contains CBSA details such as past interviews, conversation held at Port of Entries, etc.</p>
                                            <a href="{{url('/applycaips')}}" class="btn-link-primary">Apply Now</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Apply GCMS</h3>
                                            <p>GCMS stands for “Global Case Management System”. Currently most of the Canadian Offices use the GCMS system. GCMS make current or past Immigration Information available to CIC and CBSA anywhere in the world. GCMS is applicable to any type of Canada visa application which is made from within or outside of Canada.</p>
                                            <a href="{{url('/applycaips')}}" class="btn-link-primary">Apply Now</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
@endsection

@section("blog");

        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="content-area">
                            <h2>Canada Immigration Visa Information</h2>
                            
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Study Permit</h3>
                                            <p>The study permit is a document we issue that allows foreign nationals to study at designated learning institutions (DLI) in Canada. </p>
                                            <a href="#" class="btn-link-primary">Read More</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Work Permit</h3>
                                            <p>Work permits and applications for temporary workers, business people, students and caregivers in the Live-In Caregiver Program.</p>
                                            <a href="#" class="btn-link-primary">Read More</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Dependent Visa</h3>
                                            <p> The Dependent Visa for Canada  enables Canadian citizens and permanent residents  to sponsor their dependent children, parents, grandparents, spouse, common-law.</p>
                                            <a href="#" class="btn-link-primary">Read More</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Family Sponsorship</h3>
                                            <p>You can sponsor certain relatives to come to Canada if you’re at least 18 years old and a:Canadian citizen or permanent resident of Canada</p>
                                            <a href="#" class="btn-link-primary">Read More</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Immigrate to Canada</h3>
                                            <p>Express Entry provide permanent residence through Canada’s immigration system.Provincial Nominee Program (PNP) is aimed at semi and unskilled workers who have qualifications and work experience . </p>
                                            <a href="#" class="btn-link-primary">Read More</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                    <div class="card mb20">
                                        <div class="card-body">
                                            <h3>Study in Canada</h3>
                                            <p>Canada has a reputation of excellence when it comes to international studies. Canadian schools offer quality education, affordable tuition rates, and a first-class student experience.</p>
                                            <a href="#" class="btn-link-primary">Read More</a></div>
                                        <div class="slanting-pattern-small"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                    @if($message = Session::get('success'))
                                <div class="alert alert-sucess">
                                    <h3>Notice:<br> {{$message}}</h3>
                                </div>
                            @endif
                        <div class="sidebar">
                            <div class="widget widget-quote-form bg-yellow">
                                <h3 class="form-title">Free Immigration Assessment</h3>

                                <p class="form-text">Find out your options for visa by completing a free online assessment.</p>
                                <form class="sidebar-quote-form" method="post" action="/mail">
                                {{csrf_field()}}
                                     Name :
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="yourname">Your Name</label>
                                        <div class="">
                                            <input id="yourname" name="yourname" type="text" placeholder="Your Name" class="form-control" required="">
                                        </div>
                                    </div>
                                     Email :
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="email">Email</label>
                                        <div class="">
                                            <input id="email" name="email" type="email" placeholder="Email" class="form-control input-md" required="">
                                        </div>
                                    </div>
                                  Contact No:
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="mobileno">Mobile No</label>
                                        <div class="">
                                            <input id="mobileno" name="mobileno" type="number" placeholder="Mobile No" class="form-control" required="">
                                        </div>
                                    </div>
                                    Visa Type:
                                    <div class="form-group">
                                        <label for="selectvisa" class="sr-only">Select Visa</label>
                                        <select class="form-control" id="selectvisa" name="selectvisa">
                                            <option value="Select Visa">Select Visa</option>
                                            <option value="Students Visa">Students Visa</option>
                                            <option value="Business Visa">Business Visa</option>
                                            <option value="Family Visa">Family Visa</option>
                                            <option value="Travel Visa">Travel Visa </option>
                                            <option value="Work Visa">Work Visa </option>
                                            <option value="Visitor Visa">Visitor Visa </option>
                                            <option value="Migrate Visa">Migrate Visa </option>
                                            <option value="PR Visa">PR Visa</option>
                                        </select>
                                    </div>
                                    Message :
                                    <div class="form-group">
                                        <label class="control-label sr-only" for="message">Message</label>
                                        <div class="">
                                            <textarea class="form-control" id="message" name="message" rows="4" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-default btn-lg btn-block">Book Free Assessment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
