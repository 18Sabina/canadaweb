@extends("layout.html")

@section("title","Blog | Canada Visa")


@section("blog");
   
 <div class="container">
     
        @if(!empty($posts))
        <h2>Top 10 recent blogs</h2>
            @foreach($posts as $post) 
                <div class="well well-lg">
                    <h2>{{$post->title}}</h2>
                    <p>{{$post->body}}</p>
                    <p>Visit Count: {{$post->visit_count}}</p>
                    <p>Comment Count: {{$post->comment_count}}</p>
                    <p>Post Created At: {{date('F d,Y', strtotime($post->created_at))}} at {{date('g:ia',strtotime($post->created_at))}}</p>
                    <a href= "{{ route('post.show',$post->id) }}" class="btn btn-primary full-rigth">Read More</a>
                </div>
            @endforeach
        @endif    
    </div>


@endsection
