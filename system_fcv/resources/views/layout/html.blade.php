    <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
     <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <title>@yield("title")</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- jquey cd -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- added form layouts.app-->
    <script src="{{ asset('js/app.js') }}" defer></script>

<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<!-- added form layouts.app-->




    <script src="https://use.fontawesome.com/81785bbf24.js"></script>

    <!-- FontAwesome 4.0 CSS -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- Google Font CSS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700%7cPT+Serif:400,400i,700,700i" rel="stylesheet">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
 
    @section("head_section")
    @show
</head>

<body data-spy="scroll" data-target=".nav" data-offset="-20">

 <div class="topbar">
        <!-- topbar -->
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 d-none d-sm-none d-lg-block d-xl-block">
                    <p class="welcome-text">Welcome to Canada immigration company </p>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="header-block">
                        <span class="header-link d-none d-xl-block"><a href="{{url('/freeassesment')}}" class="anchor-link">Talk to Our Expert</a></span>
                        <span class="header-link"></span>
                        <span class="header-link"> <button type="submit" class="" data-toggle="modal" data-target="#searchModal"> <i class="fa fa-search"></i></button></span>
                        <span class="header-link"><a href="{{url('/applycaips')}}" class="btn btn-default btn-sm">Apply Caips Now</a></span>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12">
                      
                    <div class="header-block"> 
                        <!-- @guest
                        @endguest -->
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="header-classic">
        <!-- header classic -->
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12"><a href="index-3.html" class=""><img src="images/logo.png" alt="Canada Visa"></a></div>
                <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12">
                    <div id="navigation" class="navigation">
                        <!-- navigation -->
                        <ul>
                            <li><a href="{{url('/home')}}">Home</a>
                                <!-- <ul>
                                    <li><a href="index.html">Homepage v1</a></li>
                                    <li><a href="index-2.html">Homepage v2</a></li>
                                    <li><a href="index-3.html">Homepage v3</a></li>
                                    <li><a href="index-4.html">Homepage v4</a></li>
                                    <li><a href="index-5.html">Homepage v5</a></li>
                                    <li><a href="index-6.html">Homepage v6</a></li>
                                    <li><a href="#">Landing pages</a>
                                        <ul>
                                            <li><a href="landing-page-v1.html">Landing page v1</a></li>
                                            <li><a href="landing-page-country-v1.html">Landing page country v1</a></li>
                                            <li><a href="landing-page-ebook-v3.html">Landing page v3 - eBook</a></li>
                                            <li><a href="landing-page-webinar.html">Landing page v4 - Webinars</a></li>
                                        </ul>
                                    </li>
                                </ul> -->
                            </li>
                            <li><a href="{{url('/freeassesment')}}">Free Assesment</a>
                            </li>
                            <li><a href="#">Apply Caips</a>
                                <ul>
                                    <li><a href="{{url('/applycaips')}}">Apply CAIPS</a></li>
                                    <li><a href="{{url('/applycaips')}}">Apply FOSS</a></li>
                                    <li><a href="{{url('/applycaips')}}">Apply GCMS</a></li>

                                </ul>
                            </li>
                            <!-- <li><a href="{{url('/aboutus')}}">About us </a>
                            </li> -->
                            <li><a href="{{url('/blog')}}">Blog </a>
                            </li>
                            <li><a href="{{url('/contactus')}}">Contact us </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Auth::guard('admin')->check())
    <div class="header-classic">
        <!-- header classic -->
        <div class="container">
            <div class="row">
             
                <ul class = "nav navbar-nav navbar-right">
                    <li><a href='{{route('post.index')}}'>Managed Blog Post</a> </li>
                </ul>
            </div>
        </div>
    </div>   
    @endif 

    @section("page-header")
    @show

    @section("reject")
    @show

    @section("blog")
    @show

    @section("assesmentonly")
    @show

    <div class="footer">
        <!-- Footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5 col-md-7 col-sm-3 col-13">
                    <div class="widget-footer">
                        <h3 class="widget-title">About us</h3>
                        <p>Welcome to Canada Immigration Consulting Company</p>
                        <p>We have a global presence with a strategic network of own and associate offices across the world.We provide our services from registered Immigration Consultant. </p>
                    </div>
                </div>
                <div class="col-xl-1 col-lg-1 col-md-2 col-sm-2 col-5">
                    <!-- <div class="widget-footer">
                        <h3 class="widget-title">Country</h3>
                        <ul class="listnone arrow-footer">
                            <li><a href="#">Canada</a></li>
                            <li><a href="#">United States</a></li>
                            <li><a href="#">United Kingdom</a></li>
                            <li><a href="#">Australia</a></li>
                            <li><a href="#">New Zealand</a></li>
                            <li><a href="#">Singapore</a></li>
                            <li><a href="#">South Africa</a></li>
                            <li><a href="#">Germany</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Visas</h3>
                        <ul class="listnone arrow-footer">
                            <li><a href="{{url('/freeassesment')}}"> Students Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">Business Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">Family Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">Travel Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">Work Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">Visitor Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">Migrate Visas</a></li>
                            <li><a href="{{url('/freeassesment')}}">PR Visas</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-3 col-sm-3 col-6">
                    <div class="widget-footer">
                        <h3 class="widget-title">Contact Us</h3>
                        <strong>aesha@forcanadavisa.com</strong>
                        <!-- <address>2279 Fire Access Road
                            <br> Greensboro, NC 27401</address>
                        <p>1800 102 4150
                            <br> 1800 102 4151</p> -->
                            <br><br>
                        <p><a href="{{url('/freeassesment')}}">Schedule a Meeting</a>
                            <br>
                            <a href="{{url('/freeassesment')}}">Talk to our Expert</a></p>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-6">
                    <div class="widget-footer widget-social">
                        <h3 class="widget-title">Connect</h3>
                        <ul class="listnone">
                            <li><a href="#"><i class="fa fa-facebook social-icon"></i> Facebook</a></li>
                            <li><a href="#"><i class="fa fa-twitter social-icon"></i> Twitter</a></li>
                            <li><a href="#"><i class="fa fa-instagram social-icon"></i> Instagram</a></li>
                            <li><a href="#"><i class="fa fa-youtube social-icon"></i> Youtube</a></li>
                            <li><a href="#"><i class="fa fa-linkedin social-icon"></i> Linked In</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.footer -->
    <div class="tiny-footer">
        <!-- tiny footer -->
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <p>Copyright © 2020 <a href="/home">{{ config('app.name') }}</a>. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
   
    <div class="searchModal">
        <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control form-control-lg" placeholder="Search Here..." aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap.bundle.js"></script>
     
    <!-- menumaker js -->
    <script src="js/menumaker.js"></script>
    <script src="js/navigation.js"></script>
</body>

</html>