<?php

namespace App\Http\Controllers;

use App\Mail\ContactFormMail;
use App\Mail\ContactFormMailClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function mail(){
		$data = request()->validate([
			'yourname' => 'required',
			'email'=> 'required| email',
			'mobileno'=> 'required',
			'selectvisa'=>'required',
			'message'=>'required'
		]);
		$dataclient = array(
			'yourname' => 'your mail has been receievd:',
			'email'=> 'Your email has been received:',
			'mobileno'=> 'Your contact has been received:',
			'selectvisa'=>'Your visa type has been received:',
			'message'=>'Your message has been reached to our Lawayers. They will be in conversation with in shortly.'
		);
			
		Mail::to('sazzal77@gmail.com')->send(new ContactFormMail($data));
		Mail::to($data['email'])->send(new ContactFormMailClient($dataclient));
			return redirect('freeassesment')->with("success","Your Request has been received check you email for confrmation.We will contact you shortly.");
	}
}
