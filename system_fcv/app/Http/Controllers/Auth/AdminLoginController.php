<?php
// login form ko lage banako hola
namespace App\Http\Controllers\Auth;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    public function __construct() {
        $this->middleware('guest:admin')->except('logout'); // guest who are login as admin le form pauxa
    }  // exception becz logout fucntion use garna ne logout hunu parne vo, 
    // logout is locked into guest fucntion,so middleware le faldenxa before logout

    public function showLoginForm(){
        return view('auth.admin-login');
    }
    
    public function login(Request $request)
    {
        
        $this->validate($request, [
            'email'=> 'required|email',
            'password'=> 'required|min:8'
        ]);
        // attempt method user garne fasad bata
        if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=> $request->password],$request->remember)){
             echo("hshshshs");    
            return redirect()->intended(route('admin.dashboard'));// intended jaha jana lako, (... deefalt value if kahe jana lako tehna vanya)
        }else{
            echo "incorrect";
            return redirect()->back()->withInput($request->only('email', 'remember')); // back page jaha bata ako xa request
        }
     }

     public function logout(){
         echo "<h1>Reached</h1>";
        Auth::guard('admin')->logout();
        return redirect('/');
    }

}
