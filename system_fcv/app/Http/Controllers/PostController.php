<?php

namespace App\Http\Controllers;
use Auth;
use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $logInUserId = Auth::guard('admin')->id();
         if(!isset($logInUserId)){return("Restricted Area");}
         $posts = Post::all()->where('user_id',$logInUserId);
         return view ('adminPanel.home',['posts'=>$posts]);
        
     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ("adminPanel.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "hellow store ";
        $post = new Post;
      
        $data = request()->validate([
			'title' => 'required',
            'body'=> 'required',
            'image'=> 'image'
                   
        ]);
        print_r($data);
            
        if ($request['image'] !== NULL || isset($request['image'])) {
            $image = $data['image'];
       
            $new_name = '/images/post_images/'.rand().'.'.$image->getClientOriginalExtension();
            $image->move(public_path('images/post_images'),$new_name);
            $post->image = $new_name;
        }else{
            $post->image = "image.png";
        }

     
        $post->title = $data['title'];
        $post->body = $data['body'];
       
        $post->visit_count = '1';
      
        $post->user_id = Auth::guard('admin')->id();
        $post->save();

        return redirect()->route('post.index')->with('success','Blog has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $data = array(
            'id'=> $id,
            'post' => $post
        );
        return view('html.singleblog',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $post = Post::find($id);
        return view('adminPanel.editblog',['post' => $post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);

        if(isset($request->commentCount)){
       
            $commentCount = $request->commentCount;
            $post->comment_count = $commentCount;
        }
        if(isset($request->visitCount)){
            $visitCount = $request->visitCount;
            $post->visit_count = $visitCount;
        }
        if(isset($request->title)){
            $post->title = $request->title;
            $post->body = $request->body;
        }

        $post->save();
        return redirect()->route('post.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post-> delete();
        return redirect()->route('post.index');
    }
}
