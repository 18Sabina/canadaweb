<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','HtmlController@home');

// Route::get('home',"HtmlController@home");

Route::get('freeassesment',"HtmlController@freeassesment");

Route::get('applycaips',"HtmlController@applycaips");

Route::get('aboutus',"HtmlController@aboutus");

Route::get('blog',"HtmlController@blog");

Route::get('blogapi',"HtmlController@blogapi");

Route::get('contactus',"HtmlController@contactus");

Route::post('caipsupload', "fileupload@fileupload");

Route::post('mail', "MailController@mail");

Route::resource('post','PostController');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/user/logout', 'Auth\LoginController@userLogout')->name('user.logout');

Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@loginout')->name('admin.logout');
});

Auth::routes();