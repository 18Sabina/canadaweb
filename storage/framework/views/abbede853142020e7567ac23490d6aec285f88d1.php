<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

    <link rel="stylesheet" href = "css/app.css"> 
    </head>
    <body>
       <div id = "example"></div>
       <script src = "js/app.js"></script>
    </body>
</html>
<?php /**PATH C:\xampp\htdocs\myproject1\resources\views/welcome.blade.php ENDPATH**/ ?>